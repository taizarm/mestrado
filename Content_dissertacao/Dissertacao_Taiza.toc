\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\c c\~ao}{14}
\contentsline {section}{\numberline {1.1}Problema}{14}
\contentsline {section}{\numberline {1.2}Limita\c c\~oes das abordagens atuais}{15}
\contentsline {section}{\numberline {1.3}Objetivos}{16}
\contentsline {section}{\numberline {1.4}Contribui\c c\~oes}{16}
\contentsline {section}{\numberline {1.5}Organiza\c c\~ao do Documento}{17}
\contentsline {chapter}{\numberline {2}Fundamenta\c c\~ao Te\'orica}{18}
\contentsline {section}{\numberline {2.1}Tratamento de exce\c c\~oes}{18}
\contentsline {section}{\numberline {2.2}Tratamento de exce\c c\~oes em Java}{19}
\contentsline {section}{\numberline {2.3}\textit {Grounded Theory}}{20}
\contentsline {subsection}{\numberline {2.3.1}Vers\~oes da \textit {Grounded Theory}}{22}
\contentsline {subsection}{\numberline {2.3.2}Coleta de dados}{23}
\contentsline {subsection}{\numberline {2.3.3}Codificac\~ao}{23}
\contentsline {chapter}{\numberline {3}Estudo Explorat\'orio}{25}
\contentsline {section}{\numberline {3.1}Contexto do estudo explorat\'orio}{25}
\contentsline {section}{\numberline {3.2}Defini\c c\~ao das quest\~oes de pesquisa}{26}
\contentsline {section}{\numberline {3.3}Entrevistas semiestruturadas}{27}
\contentsline {subsection}{\numberline {3.3.1}Coleta de dados}{28}
\contentsline {subsection}{\numberline {3.3.2}Perfil dos participantes das entrevistas semiestruturadas}{30}
\contentsline {subsection}{\numberline {3.3.3}Codifica\c c\~ao}{31}
\contentsline {subsection}{\numberline {3.3.4}QP1: Na opini\~ao dos desenvolvedores, quais as vantagens e desvantagens do mecanismo de tratamento de exce\c c\~oes?}{32}
\contentsline {subsection}{\numberline {3.3.5}QP2: Quais s\~ao os obst\'aculos enfrentados pelos programadores no momento da implementa\c c\~ao do c\'odigo de tratamento de exce\c c\~oes?}{34}
\contentsline {subsection}{\numberline {3.3.6}QP3: Quais s\~ao as pr\'aticas utilizadas pelo desenvolvedor para a implementa\c c\~ao do c\'odigo de tratamento de exce\c c\~oes na ind\'ustria de software?}{36}
\contentsline {section}{\numberline {3.4}\textit {Survey} de valida\c c\~ao}{38}
\contentsline {subsection}{\numberline {3.4.1}Vantagens e desvantagens em utilizar mecanismos de tratamento de exce\c c\~oes}{39}
\contentsline {subsection}{\numberline {3.4.2}Obst\'aculos durante a implementa\c c\~ao do c\'odigo de tratamento de exce\c c\~oes}{41}
\contentsline {subsection}{\numberline {3.4.3}Pr\'aticas de desenvolvimento durante a implementa\c c\~ao do c\'odigo de tratamento de exce\c c\~oes}{43}
\contentsline {section}{\numberline {3.5}An\'alise do c\'odigo fonte e \textit {logs} do sistema}{45}
\contentsline {subsection}{\numberline {3.5.1}An\'alise est\'atica do c\'odigo fonte}{45}
\contentsline {subsection}{\numberline {3.5.2}Inspe\c c\~ao manual do c\'odigo}{47}
\contentsline {subsection}{\numberline {3.5.3}An\'alise dos \textit {logs} de exce\c c\~oes n\~ao capturadas}{47}
\contentsline {section}{\numberline {3.6}Amea\c cas \`a validade}{48}
\contentsline {section}{\numberline {3.7}Considera\c c\~oes finais}{49}
\contentsline {chapter}{\numberline {4}Ferramenta Proposta: ExceptionPolicyExpert}{51}
\contentsline {section}{\numberline {4.1}Ferramenta ECL/DAEH}{51}
\contentsline {subsection}{\numberline {4.1.1}Linguagem ECL}{52}
\contentsline {section}{\numberline {4.2}Desenvolvimento da ExceptionPolicyExpert}{53}
\contentsline {section}{\numberline {4.3}Vis\~ao geral da ExceptionPolicyExpert}{54}
\contentsline {section}{\numberline {4.4}M\'odulo de verifica\c c\~ao}{55}
\contentsline {subsection}{\numberline {4.4.1}Exemplo de pol\IeC {\'\i }tica de tratamento de exce\c c\~oes especificada em ECL}{56}
\contentsline {subsection}{\numberline {4.4.2}Sinaliza\c c\~ao indevida}{57}
\contentsline {subsection}{\numberline {4.4.3}Tratamento indevido}{58}
\contentsline {subsection}{\numberline {4.4.4}Poss\IeC {\'\i }veis tratadores}{59}
\contentsline {section}{\numberline {4.5}Limita\c c\~ao da ferramenta proposta}{61}
\contentsline {section}{\numberline {4.6}Considera\c c\~oes finais}{61}
\contentsline {chapter}{\numberline {5}Avalia\c c\~ao da Ferramenta ExceptionPolicyExpert}{63}
\contentsline {section}{\numberline {5.1}Participantes e defini\c c\~ao da pol\IeC {\'\i }tica}{63}
\contentsline {section}{\numberline {5.2}Coleta de dados do \textit {log}}{63}
\contentsline {subsection}{\numberline {5.2.1}Sinaliza\c c\~ao Indevida}{64}
\contentsline {subsection}{\numberline {5.2.2}Tratamento Indevido}{65}
\contentsline {subsection}{\numberline {5.2.3}Poss\IeC {\'\i }veis tratadores}{66}
\contentsline {section}{\numberline {5.3}Opini\~ao dos usu\'arios}{67}
\contentsline {section}{\numberline {5.4}Considera\c c\~oes finais}{68}
\contentsline {chapter}{\numberline {6}Trabalhos relacionados}{69}
\contentsline {section}{\numberline {6.1}Estudos qualitativos envolvendo desenvolvedores}{69}
\contentsline {section}{\numberline {6.2}Ferramentas para defini\c c\~ao e checagem de pol\IeC {\'\i }ticas de tratamento de exce\c c\~oes}{71}
\contentsline {section}{\numberline {6.3}Ferramentas de recomenda\c c\~ao}{73}
\contentsline {chapter}{\numberline {7}Considera\c c\~oes Finais}{75}
\contentsline {section}{\numberline {7.1}Trabalhos futuros}{76}
\contentsline {chapter}{Refer\^encias}{78}
\contentsline {chapter}{Ap\^endice{} A{} -$\tmspace -\thinmuskip {.1667em}$-{} Roteiro base das entrevistas}{81}
\contentsline {chapter}{Ap\^endice{} B{} -$\tmspace -\thinmuskip {.1667em}$-{} C\'odigos mantidos ap\'os a codifica\c c\~ao seletiva}{83}
\contentsline {chapter}{Ap\^endice{} C{} -$\tmspace -\thinmuskip {.1667em}$-{} \textit {Survey} de valida\c c\~ao}{86}
\contentsline {chapter}{Ap\^endice{} D{} -$\tmspace -\thinmuskip {.1667em}$-{} Defini\c c\~ao da pol\IeC {\'\i }tica utilizada na avalia\c c\~ao da ferramenta}{89}
\contentsline {chapter}{Ap\^endice{} E{} -$\tmspace -\thinmuskip {.1667em}$-{} Avalia\c c\~ao da ferramenta ExcpetionPolicyExpert}{92}
