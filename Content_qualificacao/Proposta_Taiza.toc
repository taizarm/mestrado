\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\c c\~ao}{9}
\contentsline {section}{\numberline {1.1}Problema}{10}
\contentsline {section}{\numberline {1.2}Limita\c c\~oes das abordagens atuais}{10}
\contentsline {section}{\numberline {1.3}Objetivos}{11}
\contentsline {section}{\numberline {1.4}Contribui\c c\~oes}{11}
\contentsline {section}{\numberline {1.5}Organiza\c c\~ao do Documento}{11}
\contentsline {chapter}{\numberline {2}Fundamenta\c c\~ao Te\'orica}{13}
\contentsline {section}{\numberline {2.1}Tratamento de exce\c c\~oes}{13}
\contentsline {section}{\numberline {2.2}Tratamento de exce\c c\~oes em Java}{14}
\contentsline {section}{\numberline {2.3}\textit {Grounded Theory}}{15}
\contentsline {subsection}{\numberline {2.3.1}Vers\~oes da \textit {Grounded Theory}}{17}
\contentsline {subsection}{\numberline {2.3.2}Coleta de dados}{18}
\contentsline {subsection}{\numberline {2.3.3}Codificac\~ao}{18}
\contentsline {chapter}{\numberline {3}Estudo Qualitativo}{19}
\contentsline {section}{\numberline {3.1}Metodologia}{19}
\contentsline {subsection}{\numberline {3.1.1}Defini\c c\~ao das quest\~oes do \textit {survey} explorat\'orio}{19}
\contentsline {subsection}{\numberline {3.1.2}Coleta de dados}{20}
\contentsline {subsubsection}{\numberline {3.1.2.1}Perfil dos participantes}{22}
\contentsline {subsection}{\numberline {3.1.3}Codifica\c c\~ao}{23}
\contentsline {section}{\numberline {3.2}Resultados}{24}
\contentsline {subsection}{\numberline {3.2.1}Q1: Na opini\~ao dos desenvolvedores, quais as vantagens e desvantagens do mecanismo de tratamento de exce\c c\~oes?}{24}
\contentsline {subsection}{\numberline {3.2.2}Q2: Quais s\~ao os obst\'aculos enfrentados pelos programadores no momento da implementa\c c\~ao do c\'odigo do tratamento de exce\c c\~oes?}{27}
\contentsline {subsection}{\numberline {3.2.3}Q3: Quais s\~ao as pr\'aticas utilizadas pelo desenvolvedor para o tratamento de exce\c c\~oes na ind\'ustria de software?}{28}
\contentsline {section}{\numberline {3.3}Considera\c c\~oes finais}{31}
\contentsline {chapter}{\numberline {4}Ferramenta Proposta: ExceptionPolicyExpert}{32}
\contentsline {section}{\numberline {4.1}Ferramenta ECL/DAEH}{33}
\contentsline {subsection}{\numberline {4.1.1}Linguagem ECL}{33}
\contentsline {section}{\numberline {4.2}Ferramenta PLEA}{34}
\contentsline {section}{\numberline {4.3}Desenvolvimento da ExceptionPolicyExpert}{35}
\contentsline {section}{\numberline {4.4}Vis\~ao Geral da ExceptionPolicyExpert}{36}
\contentsline {section}{\numberline {4.5}M\'odulo de recomenda\c c\~ao}{37}
\contentsline {subsection}{\numberline {4.5.1}Sinaliza\c c\~ao Indevida}{38}
\contentsline {subsection}{\numberline {4.5.2}Tratamento Indevido}{39}
\contentsline {subsection}{\numberline {4.5.3}Tratamento Indevido para par Sinalizador-Tratador}{39}
\contentsline {subsection}{\numberline {4.5.4}Poss\IeC {\'\i }veis tratadores}{40}
\contentsline {chapter}{\numberline {5}Cap\IeC {\'\i }tulo 5}{42}
\contentsline {section}{\numberline {5.1}Planejamento do Estudo}{42}
\contentsline {subsection}{\numberline {5.1.1}Hip\'oteses}{42}
\contentsline {subsection}{\numberline {5.1.2}Vari\'aveis de pesquisa}{43}
\contentsline {subsection}{\numberline {5.1.3}Sele\c c\~ao dos participantes}{45}
\contentsline {subsection}{\numberline {5.1.4}Design do Estudo}{45}
\contentsline {section}{\numberline {5.2}Defini\c c\~ao do estudo}{46}
\contentsline {section}{\numberline {5.3}Coleta de dados e interpreta\c c\~ao dos resultados}{46}
\contentsline {chapter}{\numberline {6}Trabalhos relacionados}{47}
\contentsline {section}{\numberline {6.1}Estudos qualitativos envolvendo desenvolvedores}{47}
\contentsline {section}{\numberline {6.2}Ferramentas para defini\c c\~ao e checagem de pol\IeC {\'\i }ticas de tratamento de exce\c c\~oes}{49}
\contentsline {section}{\numberline {6.3}Ferramentas de recomenda\c c\~ao}{51}
\contentsline {chapter}{\numberline {7}Considera\c c\~oes Finais e Planejamento}{53}
\contentsline {chapter}{Refer\^encias}{56}
\contentsline {chapter}{Ap\^endice{} A{} -$\tmspace -\thinmuskip {.1667em}$-{} Roteiro base das entrevistas}{58}
\contentsline {chapter}{Ap\^endice{} B{} -$\tmspace -\thinmuskip {.1667em}$-{} C\'odigos mantidos ap\'os a codifica\c c\~ao seletiva}{60}
