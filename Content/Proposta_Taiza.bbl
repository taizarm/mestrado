\providecommand{\abntreprintinfo}[1]{%
 \citeonline{#1}}
\setlength{\labelsep}{0pt}\begin{thebibliography}{}
\providecommand{\abntrefinfo}[3]{}
\providecommand{\abntbstabout}[1]{}
\abntbstabout{1.45.2.1 }

\bibitem[Abrantes e Coelho 2015]{abrantes2015specifying}
\abntrefinfo{Abrantes e Coelho}{ABRANTES; COELHO}{2015}
{ABRANTES, J.; COELHO, R. Specifying and dynamically monitoring the exception
  handling policy. In:  \emph{Proceedings of the 27th International Conference
  on Software Engineering and Knowledge Engineering}. [S.l.: s.n.], 2015. p.
  370--374.}

\bibitem[Abrantes 2016]{abrantes2016tese}
\abntrefinfo{Abrantes}{ABRANTES}{2016}
{ABRANTES, J. V.
\emph{Especifica��o e Monitoramento Din�mico da Pol�tica de Tratamento de
  Exce��es}.
Disserta��o (Mestrado) --- Universidade Federal do Rio Grande do Norte, 2016.}

\bibitem[Barbosa et al. s.d]{barbosaenforcing}
\abntrefinfo{Barbosa et al.}{BARBOSA et al.}{s.d}
{BARBOSA, E. et al. Enforcing exception handling policies with a
  domain-specific language.
IEEE, s.d.}

\bibitem[Barbosa 2015]{barbosa2015mastering}
\abntrefinfo{Barbosa}{BARBOSA}{2015}
{BARBOSA, E.~A. Mastering global exceptions with policy-aware recommendations.
  In:  IEEE PRESS. \emph{Proceedings of the 37th International Conference on
  Software Engineering-Volume 2}. [S.l.], 2015. p. 778--780.}

\bibitem[Barbosa, Garcia e Mezini 2012]{barbosa2012heuristic}
\abntrefinfo{Barbosa, Garcia e Mezini}{BARBOSA; GARCIA; MEZINI}{2012}
{BARBOSA, E.~A.; GARCIA, A.; MEZINI, M. Heuristic strategies for recommendation
  of exception handling code. In:  IEEE. \emph{Software Engineering (SBES),
  2012 26th Brazilian Symposium on}. [S.l.], 2012. p. 171--180.}

\bibitem[Basili, Caldiera e Rombach 1994]{basili1994encyclopedia}
\abntrefinfo{Basili, Caldiera e Rombach}{BASILI; CALDIERA; ROMBACH}{1994}
{BASILI, V.; CALDIERA, G.; ROMBACH, H.~D. Encyclopedia of software engineering.
John Wiley \& Sons, Inc., 1994.}

\bibitem[Charmaz 2014]{charmaz2014constructing}
\abntrefinfo{Charmaz}{CHARMAZ}{2014}
{CHARMAZ, K. \emph{Constructing grounded theory}. [S.l.]: Sage, 2014.}

\bibitem[Coelho 2008]{de2008analyzing}
\abntrefinfo{Coelho}{COELHO}{2008}
{COELHO, R. de S.
\emph{Analyzing Exception Flows of Aspect-Oriented Programs}.
Tese (Doutorado) --- PUC-Rio, 2008.}

\bibitem[Ebert e Castor 2013]{ebert2013study}
\abntrefinfo{Ebert e Castor}{EBERT; CASTOR}{2013}
{EBERT, F.; CASTOR, F. A study on developers' perceptions about exception
  handling bugs. In:  \emph{ICSM}. [S.l.: s.n.], 2013. p. 448--451.}

\bibitem[Glaser et al. 1967]{glaser1967discovery}
\abntrefinfo{Glaser et al.}{GLASER et al.}{1967}
{GLASER, B. G.~S. et al. \emph{The discovery of grounded theorystrategies for
  qualitative research}.
[S.l.], 1967.}

\bibitem[Greiler, Deursen e Storey 2012]{greiler2012test}
\abntrefinfo{Greiler, Deursen e Storey}{GREILER; DEURSEN; STOREY}{2012}
{GREILER, M.; DEURSEN, A. van; STOREY, M.-A. Test confessions: a study of
  testing practices for plug-in systems. In:  IEEE. \emph{2012 34th
  International Conference on Software Engineering (ICSE)}. [S.l.], 2012. p.
  244--254.}

\bibitem[Hunter, Hunter et al. 1978]{hunter1978statistics}
\abntrefinfo{Hunter, Hunter et al.}{HUNTER; HUNTER et al.}{1978}
{HUNTER, W.~G.; HUNTER, J.~S. et al. \emph{Statistics for experimenters: an
  introduction to design, data analysis, and model building}. [S.l.]: Wiley New
  York, 1978.}

\bibitem[Kienzle 2008]{kienzle2008exceptions}
\abntrefinfo{Kienzle}{KIENZLE}{2008}
{KIENZLE, J. On exceptions and the software development life cycle. In:  ACM.
  \emph{Proceedings of the 4th international workshop on Exception handling}.
  [S.l.], 2008. p. 32--38.}

\bibitem[Laprie 1985]{laprie1985dependable}
\abntrefinfo{Laprie}{LAPRIE}{1985}
{LAPRIE, J.-C. Dependable computing and fault-tolerance.
\emph{Digest of Papers FTCS-15}, p. 2--11, 1985.}

\bibitem[Lemos e Romanovsky 2001]{de2001exception}
\abntrefinfo{Lemos e Romanovsky}{LEMOS; ROMANOVSKY}{2001}
{LEMOS, R. de; ROMANOVSKY, A. Exception handling in the software lifecycle.
\emph{International Journal of Computer Systems Science and Engineering}, CRL
  Publishing, v.~16, n.~2, p. 167--181, 2001.}

\bibitem[Melo et al. 2013]{melo2013depth}
\abntrefinfo{Melo et al.}{MELO et al.}{2013}
{MELO, H. et al. In-depth characterization of exception flows in software
  product lines: an empirical study.
\emph{Journal of Software Engineering Research and Development}, Springer
  Berlin Heidelberg, v.~1, n.~1, p.~1, 2013.}

\bibitem[Montoni 2010]{montoni2010investigaccao}
\abntrefinfo{Montoni}{MONTONI}{2010}
{MONTONI, M.~A.
\emph{Uma investiga{\c{c}}{\~a}o sobre os fatores cr{\'\i}ticos de sucesso em
  iniciativas de melhoria de processos de software}.
Tese (Doutorado) --- Universidade Federal do Rio de Janeiro, 2010.}

\bibitem[Ng e Hase 2008]{ng2008grounded}
\abntrefinfo{Ng e Hase}{NG; HASE}{2008}
{NG, K.; HASE, S. Grounded suggestions for doing a grounded theory business
  research.
\emph{Electronic Journal of Business Research Methods}, v.~6, n.~2, p.
  155--170, 2008.}

\bibitem[Radatz, Geraci e Katki 1990]{radatz1990ieee}
\abntrefinfo{Radatz, Geraci e Katki}{RADATZ; GERACI; KATKI}{1990}
{RADATZ, J.; GERACI, A.; KATKI, F. Ieee standard glossary of software
  engineering terminology.
\emph{IEEE Std}, v. 610121990, n. 121990, p.~3, 1990.}

\bibitem[Rahman e Roy 2014]{rahman2014use}
\abntrefinfo{Rahman e Roy}{RAHMAN; ROY}{2014}
{RAHMAN, M.~M.; ROY, C.~K. On the use of context in recommending exception
  handling code examples. In:  \emph{SCAM}. [S.l.: s.n.], 2014. p. 285--294.}

\bibitem[Shah, G{\"o}rg e Harrold 2008]{shah2008visualization}
\abntrefinfo{Shah, G{\"o}rg e Harrold}{SHAH; G{\"O}RG; HARROLD}{2008a}
{SHAH, H.; G{\"O}RG, C.; HARROLD, M.~J. Visualization of exception handling
  constructs to support program understanding. In:  ACM. \emph{Proceedings of
  the 4th ACM symposium on Software visualization}. [S.l.], 2008. p. 19--28.}

\bibitem[Shah, G{\"o}rg e Harrold 2008]{shah2008developers}
\abntrefinfo{Shah, G{\"o}rg e Harrold}{SHAH; G{\"O}RG; HARROLD}{2008b}
{SHAH, H.; G{\"O}RG, C.; HARROLD, M.~J. Why do developers neglect exception
  handling? In:  ACM. \emph{Proceedings of the 4th international workshop on
  Exception handling}. [S.l.], 2008. p. 62--68.}

\bibitem[Shah, Gorg e Harrold 2010]{shah2010understanding}
\abntrefinfo{Shah, Gorg e Harrold}{SHAH; GORG; HARROLD}{2010}
{SHAH, H.; GORG, C.; HARROLD, M.~J. Understanding exception handling:
  Viewpoints of novices and experts.
\emph{IEEE Transactions on Software Engineering}, IEEE, v.~36, n.~2, p.
  150--161, 2010.}

\bibitem[Stol, Ralph e Fitzgerald 2016]{stol2016grounded}
\abntrefinfo{Stol, Ralph e Fitzgerald}{STOL; RALPH; FITZGERALD}{2016}
{STOL, K.-J.; RALPH, P.; FITZGERALD, B. Grounded theory in software engineering
  research: a critical review and guidelines. In:  ACM. \emph{Proceedings of
  the 38th International Conference on Software Engineering}. [S.l.], 2016. p.
  120--131.}

\bibitem[Wirfs-Brock 2006]{wirfs2006toward}
\abntrefinfo{Wirfs-Brock}{WIRFS-BROCK}{2006}
{WIRFS-BROCK, R.~J. Toward exception-handling best practices and patterns.
\emph{IEEE software}, IEEE, v.~23, n.~5, p. 11--13, 2006.}

\end{thebibliography}
